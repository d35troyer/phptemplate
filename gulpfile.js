var gulp        = require('gulp');
var browserSync = require('browser-sync').create();

gulp.task('default', function() {
    browserSync.init({
				proxy: "mactemplate:8888",
				open: false,
				notify: false,
    });
		gulp.watch("./www/*.php").on("change", browserSync.reload);
		gulp.watch("./www/**/*.css").on("change", browserSync.reload);
});